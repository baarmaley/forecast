
#include <QCoreApplication>
#include <QFile>
#include <QTimer>
#include <QWebSocket>
#include <QWebSocketServer>

#include <common/overloaded.hpp>

#include <protocol/request.hpp>
#include <protocol/response.hpp>

#include <country.hpp>
#include <flag.hpp>
#include <icon.hpp>

#include <variant>

using namespace forecast;

void deleterForQObject(QObject* p)
{
    p->deleteLater();
}

struct Client
{
    Client(QWebSocket* socket) : socket(socket, deleterForQObject) {}
    std::unique_ptr<QWebSocket, void (*)(QObject*)> socket;
    std::size_t sizePacket{0};
    std::size_t receivedBytes{0};
};

std::string readPictureFromResource(const char* path)
{
    QFile file(path);
    if (file.open(QIODevice::ReadOnly)) {
        return file.readAll().toBase64().toStdString();
    }
    return "";
}

int main(int argc, char* argv[])
{
    QCoreApplication app(argc, argv);

    std::unordered_map<std::uint32_t, Client> m_clients;
    std::uint32_t counter_client_id{0};

    std::vector<core::api::ForecastData> forecastData;

    for (std::uint32_t i = 0; i < 30; ++i) {
        forecastData.push_back(core::api::ForecastData{static_data::country_array[i],
                                                       static_cast<float>((i % 2 == 0) ? i : -static_cast<float>(i)),
                                                       static_cast<core::api::FlagId>(i),
                                                       static_cast<core::api::IconId>(i % 5)});
    }

    QWebSocketServer server("ForecastServer", QWebSocketServer::NonSecureMode);
    if (server.listen(QHostAddress("0.0.0.0"), 33470)) {
        qDebug() << server.serverPort();
        QObject::connect(&server, &QWebSocketServer::newConnection, &server, [&] {
            qDebug() << "newConnection";
            QWebSocket* pSocket = server.nextPendingConnection();

            ++counter_client_id;
            auto id = counter_client_id;

            m_clients.emplace(std::piecewise_construct, std::forward_as_tuple(id), std::forward_as_tuple(pSocket));

            QObject::connect(pSocket, &QWebSocket::disconnected, pSocket, [id, &m_clients] { m_clients.erase(id); });
            QObject::connect(pSocket,
                             &QWebSocket::binaryMessageReceived,
                             pSocket,
                             [id, &m_clients, &forecastData](QByteArray bytes) {
                                 qDebug() << "recieve Bytes" << bytes;
                                 try {
                                     auto j          = nlohmann::json::parse(qUncompress(bytes).toStdString());
                                     auto request    = j.at(0).get<core::protocol::Request>();
                                     auto request_id = request.requestId;
                                     auto s = std::chrono::seconds{10 + (static_cast<std::uint32_t>(request_id) % 5)};

                                     auto response = std::visit(
                                         common::overloaded{
                                             [request_id, &forecastData](core::protocol::Hello) {
                                                 return core::protocol::response(request_id, forecastData);
                                             },
                                             [request_id, &s](core::api::FlagId flagId) {
                                                 return core::protocol::response(
                                                     request_id,
                                                     flagId,
                                                     s,
                                                     readPictureFromResource(
                                                         static_data::flag_array[static_cast<std::uint32_t>(flagId)]));
                                             },
                                             [request_id, &s](core::api::IconId iconId) {
                                                 return core::protocol::response(
                                                     request_id,
                                                     iconId,
                                                     s,
                                                     readPictureFromResource(
                                                         static_data::icon_array[static_cast<std::uint32_t>(iconId)]));
                                             }},
                                         request.data);

                                     auto bytes = qCompress(
                                         QByteArray::fromStdString(nlohmann::json{std::move(response)}.dump()), 9);

                                     QTimer::singleShot(0, [id, &m_clients, bytes = std::move(bytes)] {
                                         auto it = m_clients.find(id);
                                         if (it == m_clients.end()) {
                                             qDebug() << "Client not found";
                                             return;
                                         }
                                         it->second.socket->sendBinaryMessage(bytes);
                                     });

                                 } catch (std::exception& e) {
                                     // Нарушение протокола, разрываем соединение
                                     qDebug() << e.what();
                                     auto it = m_clients.find(id);
                                     if (it == m_clients.end()) {
                                         return;
                                     }
                                     it->second.socket->close();
                                 }
                             });
        });
        QObject::connect(&server, &QWebSocketServer::closed, &server, [] {});
    }

    return app.exec();
}