#pragma once

#include <QString>

#include <variant>

namespace forecast::gui {
struct ResourceUrl
{
    QString url;
};
struct ProviderUrl
{
    QString url;
};

using TypeUrl = std::variant<ProviderUrl, ResourceUrl>;

} // namespace forecast::gui