#pragma once

#include <common/event.hpp>

#include <api/forecast_data.hpp>
#include <image_cache/flag_cache_fwd.hpp>
#include <image_cache/icon_cache_fwd.hpp>

#include <QAbstractListModel>

#include <vector>

namespace forecast::gui {
enum ForecastListRoles
{
    fm_name_country = Qt::DisplayRole + 1,
    fm_temperature,
    fm_flag_id,
    fm_icon_id,
};
class ForecastList : public QAbstractListModel
{
    Q_OBJECT
public:
    // View �� cache
    ForecastList(FlagCache& flagCache, IconCache& iconCache);
    ~ForecastList();

    int rowCount(const QModelIndex& = QModelIndex()) const override;
    QVariant data(const QModelIndex&, int) const override;

    void pushBack(core::api::ForecastData data);
	void setItems(std::vector<core::api::ForecastData> items);

protected:
    inline QHash<int, QByteArray> roleNames() const override
    {
        return m_roleNames;
    }

    template<typename Id, typename F>
    void updateRows(Id id, int role, F&& f)
    {
        std::vector<int> rows;
        for (std::size_t i = 0; i < items.size(); ++i) {
            if (std::invoke(std::forward<F>(f), items[i]) == id) {
                rows.push_back(static_cast<int>(i));
            }
        }
        for (const auto& row : rows) {
            auto cIndex = index(row, 0);
            dataChanged(cIndex, cIndex, {role});
        }
    }

private:
    FlagCache& flagCache;
    IconCache& iconCache;
    QHash<int, QByteArray> m_roleNames;
    std::vector<core::api::ForecastData> items;
    common::SubscribersContainer subscribers;
};
} // namespace forecast::gui