#include "forecast_list.hpp"

#include <image_cache/flag_cache.hpp>
#include <image_cache/icon_cache.hpp>
#include <image_cache/load_placeholder.hpp>

#include <QDebug>

namespace forecast::gui {

ForecastList::ForecastList(FlagCache& flagCache, IconCache& iconCache) : flagCache(flagCache), iconCache(iconCache)
{
    m_roleNames[ForecastListRoles::fm_name_country] = "fm_name_country";
    m_roleNames[ForecastListRoles::fm_temperature]  = "fm_temperature";
    m_roleNames[ForecastListRoles::fm_flag_id]      = "fm_flag_id";
    m_roleNames[ForecastListRoles::fm_icon_id]      = "fm_icon_id";

    subscribers += flagCache.update().subscribe([this](core::api::FlagId flagId) {
        updateRows(flagId, ForecastListRoles::fm_flag_id, &core::api::ForecastData::flag_id);
    });

    subscribers += iconCache.update().subscribe([this](core::api::IconId iconId) {
        updateRows(iconId, ForecastListRoles::fm_icon_id, &core::api::ForecastData::icon_id);
    });
}
ForecastList::~ForecastList() = default;
int ForecastList::rowCount(const QModelIndex& parent) const
{
    return parent.isValid() ? 0 : static_cast<int>(items.size());
}
QVariant ForecastList::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }
    if (role != Qt::DisplayRole && !m_roleNames.contains(role)) {
        return QVariant();
    }

    const auto& value = items[index.row()];

    switch (static_cast<ForecastListRoles>(role)) {
        case ForecastListRoles::fm_name_country: {
            return QString::fromStdString(value.name_country);
        }
        case ForecastListRoles::fm_temperature: {
            return QString::number(value.temperature);
        }
        case ForecastListRoles::fm_flag_id: {
            return flagCache.getUrl(value.flag_id);
        }
        case ForecastListRoles::fm_icon_id: {
            return iconCache.getUrl(value.icon_id);
        }
        default: {
            qWarning() << "ForecastModelRoles does not found";
            return QVariant();
        }
    }
}
void ForecastList::pushBack(core::api::ForecastData data)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    items.push_back(std::move(data));
    endInsertRows();
}
void ForecastList::setItems(std::vector<core::api::ForecastData> items)
{
    if (!this->items.empty()) {
        beginRemoveRows(QModelIndex(), 0, rowCount() - 1);
        this->items.clear();
        endRemoveRows();
    }
    if (!items.empty()) {
        beginInsertRows(QModelIndex(), 0, static_cast<int>(items.size()) - 1);
        this->items = std::move(items);
        endInsertRows();
    }
}
} // namespace forecast::gui
