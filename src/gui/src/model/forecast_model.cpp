#include "forecast_model.hpp"

#include <model/forecast_list.hpp>

namespace forecast::gui {
ForecastModel::ForecastModel(FlagCache& flagCache, IconCache& iconCache) : m_forecastList(flagCache, iconCache) {}
void ForecastModel::setTxStatistics(CompressStatistics statistics)
{
    tx_real_size     = statistics.real;
    tx_compress_size = statistics.compress;
	emit txRealChanged(tx_real_size);
	emit txCompressChanged(tx_compress_size);
}
void ForecastModel::setRxStatistics(CompressStatistics statistics)
{
	rx_real_size = statistics.real;
	rx_compress_size = statistics.compress;

	emit rxRealChanged(rx_real_size);
	emit rxCompressChanged(rx_compress_size);
}
} // namespace forecast::gui