#pragma once

#include <image_cache/flag_cache_fwd.hpp>
#include <image_cache/icon_cache_fwd.hpp>
#include <model/forecast_list.hpp>
#include <server_service/compress_statistics.hpp>

#include <QObject>

namespace forecast::gui {
class ForecastModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(ForecastList* forecastList READ forecastList NOTIFY forecastListChanged);

    Q_PROPERTY(qlonglong tx_real MEMBER tx_real_size NOTIFY txRealChanged);
    Q_PROPERTY(qlonglong tx_compress MEMBER tx_compress_size NOTIFY txCompressChanged);

    Q_PROPERTY(qlonglong rx_real MEMBER rx_real_size NOTIFY rxRealChanged);
    Q_PROPERTY(qlonglong rx_compress MEMBER rx_compress_size NOTIFY rxCompressChanged);

public:
    ForecastModel(FlagCache& flagCache, IconCache& iconCache);

    inline ForecastList* forecastList()
    {
        return &m_forecastList;
    }

	void setTxStatistics(CompressStatistics statistics);
	void setRxStatistics(CompressStatistics statistics);

signals:
    void forecastListChanged(ForecastList*);

    void txRealChanged(qlonglong);
	void txCompressChanged(qlonglong);

    void rxRealChanged(qlonglong);
	void rxCompressChanged(qlonglong);


private:
    std::uint64_t tx_real_size{0};
    std::uint64_t tx_compress_size{0};

    std::uint64_t rx_real_size{0};
    std::uint64_t rx_compress_size{0};

    ForecastList m_forecastList;
};
} // namespace forecast::gui