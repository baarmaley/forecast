#include "server_service.hpp"

#include <common/overloaded.hpp>

#include <QTimer>

namespace forecast::gui {
ServerService::ServerService()
{
    QObject::connect(&webSocket, &QWebSocket::connected, &webSocket, [ctx_ptr = ctx.ptr()] {
        qDebug() << "Connect";
        auto self_ptr = ctx_ptr.lock();
        if (!self_ptr) {
            return;
        }
        auto self = *self_ptr;

        if (!self->tasks.empty()) {
            for (auto& item : self->tasks) {
                item.second.on_error("new connection");
            }
            self->tasks.clear();
        }
        self->helloResponseEvent({});

        auto request_id = self->requestId();

        auto json = nlohmann::json{core::protocol::request(request_id, core::protocol::Hello{})};

        self->sendBinaryMessage(json.dump());
    });
    QObject::connect(&webSocket,
                     QOverload<QAbstractSocket::SocketError>::of(&QWebSocket::error),
                     [ctx_ptr = ctx.ptr()](QAbstractSocket::SocketError error) {
                         qDebug() << error;
                         auto self = ctx_ptr.lock();
                         if (!self) {
                             return;
                         }
                         (*self)->reconnect();
                     });

    QObject::connect(&webSocket, &QWebSocket::disconnected, &webSocket, [ctx_ptr = ctx.ptr()] {
        qDebug() << "disconnected";
        auto self = ctx_ptr.lock();
        if (!self) {
            return;
        }
        (*self)->reconnect();
    });

    QObject::connect(
        &webSocket, &QWebSocket::binaryMessageReceived, &webSocket, [ctx_ptr = ctx.ptr()](QByteArray bytes) {
            qDebug() << "received";
            auto self_ptr = ctx_ptr.lock();
            if (!self_ptr) {
                return;
            }
            auto self = *self_ptr;
            self->rx_compress_size += bytes.size();
            auto buffer = qUncompress(bytes);
            self->rx_real_size += buffer.size();

            self->rxStatisticsEvent(CompressStatistics{self->rx_real_size, self->rx_compress_size});

            try {
                auto j        = nlohmann::json::parse(buffer.toStdString());
                auto response = j.at(0).get<core::protocol::Response>();

                std::visit(common::overloaded{[&](core::protocol::HelloResponse& hello) {
                                                  self->helloResponseEvent(std::move(hello.data));
                                              },
                                              [&](auto& value) {
                                                  auto it = self->tasks.find(response.requestId);
                                                  if (it == self->tasks.end()) {
                                                      return;
                                                  }
                                                  it->second.on_success(value.data, value.expire);
                                              }},
                           response.data);
            } catch (std::exception& e) {
                qDebug() << e.what();
                self->webSocket.close();
            }
        });
}
void ServerService::connectToServer()
{
    webSocket.open(QUrl("ws://localhost:33470"));
}
void ServerService::reconnect()
{
    QTimer::singleShot(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::seconds{2}).count(),
                       [ctx_ptr = ctx.ptr()] {
                           qDebug() << "Reconnect";

                           auto self = ctx_ptr.lock();
                           if (!self) {
                               return;
                           }
                           auto state = (*self)->webSocket.state();
                           if (state == QAbstractSocket::ConnectedState || state == QAbstractSocket::ConnectingState) {
                               qDebug() << "Socket already connected.";
                               return;
                           }
                           (*self)->connectToServer();
                       });
}
void ServerService::sendBinaryMessage(std::string bytes)
{
    tx_real_size += bytes.size();

    auto buffer = qCompress(QByteArray(bytes.c_str(), bytes.size()), 9);

    tx_compress_size += buffer.size();

    webSocket.sendBinaryMessage(std::move(buffer));

    txStatisticsEvent(CompressStatistics{tx_real_size, tx_compress_size});
}
} // namespace forecast::gui