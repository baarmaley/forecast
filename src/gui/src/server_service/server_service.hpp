#pragma once

#include <common/context.hpp>
#include <common/event.hpp>

#include <api/forecast_data.hpp>
#include <api/request_id.hpp>

#include <protocol/request.hpp>
#include <protocol/response.hpp>

#include <server_service/compress_statistics.hpp>

#include <QFile>
#include <QWebSocket>

namespace forecast::gui {
using OnSuccessFn = std::function<void(std::string bytes, std::chrono::seconds expire)>;
using OnErrorFn   = std::function<void(std::string)>;

struct Task
{
    Task(OnSuccessFn on_success, OnErrorFn on_error) : on_success(std::move(on_success)), on_error(std::move(on_error))
    {
    }
    OnSuccessFn on_success;
    OnErrorFn on_error;
};

class ServerService
{
public:
    ServerService();
    ServerService(const ServerService&) = delete;
    ServerService(ServerService&&)      = delete;

    void connectToServer();

    template<typename Id>
    core::api::RequestId request(Id id,
                                 std::function<void(std::string bytes, std::chrono::seconds expire)> on_success,
                                 std::function<void(std::string)> on_error)
    {
        auto request_id = requestId();

        tasks.emplace(std::piecewise_construct,
                      std::forward_as_tuple(request_id),
                      std::forward_as_tuple(std::move(on_success), std::move(on_error)));

        auto json = nlohmann::json{core::protocol::request(request_id, id)};
        sendBinaryMessage(json.dump());

        return request_id;
    }

    inline const auto& helloResponse() const
    {
        return helloResponseEvent;
    }

	inline const auto& txStatistics() const
	{
		return txStatisticsEvent;
	}

	inline const auto& rxStatistics() const
	{
		return rxStatisticsEvent;
	}

private:
    void reconnect();

    void sendBinaryMessage(std::string bytes);

    core::api::RequestId requestId()
    {
        counterRequest = static_cast<core::api::RequestId>(static_cast<std::uint32_t>(counterRequest) + 1);
        return counterRequest;
    }

    common::Event<std::vector<core::api::ForecastData>> helloResponseEvent;
	common::Event<CompressStatistics> txStatisticsEvent;
	common::Event<CompressStatistics> rxStatisticsEvent;

    std::unordered_map<core::api::RequestId, Task> tasks;
    core::api::RequestId counterRequest{0};

    std::uint64_t tx_real_size{0};
    std::uint64_t tx_compress_size{0};

    std::uint64_t rx_real_size{0};
    std::uint64_t rx_compress_size{0};

    QWebSocket webSocket;
    common::Context<ServerService> ctx{this};
};

// class ServerService
//{
// public:
//    template<typename Request>
//    core::api::RequestId request(Request request,
//                                 std::function<void(std::string bytes, std::chrono::seconds expire)> on_success,
//                                 std::function<void(std::string)> on_error)
//    {
//        auto id = requestId();
//
//        QTimer::singleShot(10000, [on_success = std::move(on_success), on_error = std::move(on_error)] {
//            QFile file(":/loading.png");
//            if (file.open(QIODevice::ReadOnly)) {
//                on_success(file.readAll().toStdString(), std::chrono::seconds{20});
//            } else {
//                on_error("file not opening");
//            }
//        });
//
//        // QTimer::singleShot(2000, [on_error = std::move(on_error)] { on_error("test"); });
//
//        return id;
//    };
//
// private:
//    core::api::RequestId requestId()
//    {
//        counterRequest = static_cast<core::api::RequestId>(static_cast<std::uint32_t>(counterRequest) + 1);
//        return counterRequest;
//    }
//
//    core::api::RequestId counterRequest{0};
//};
} // namespace forecast::gui