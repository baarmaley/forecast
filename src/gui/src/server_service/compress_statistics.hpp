#pragma once

#include <cstdint>

namespace forecast::gui {

struct CompressStatistics {
	std::uint64_t real;
	std::uint64_t compress;
};

}