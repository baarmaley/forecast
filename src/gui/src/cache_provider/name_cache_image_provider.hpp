#pragma once

namespace forecast::gui {
constexpr char name_cache_image_provider[] = "cache_image_provider";
} // namespace forecast::gui