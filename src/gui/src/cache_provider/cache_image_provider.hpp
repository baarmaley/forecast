#pragma once

#include <file_service/file_service_fwd.hpp>
#include <cache_provider/name_cache_image_provider.hpp>

#include <QQuickImageProvider>

namespace forecast::gui {
class CacheImageProvider : public QQuickImageProvider
{
public:
    CacheImageProvider(FileService& fileService);

    QPixmap requestPixmap(const QString& id, QSize* size, const QSize& requestedSize) override;

private:
    FileService& fileService;
};
} // namespace forecast::gui