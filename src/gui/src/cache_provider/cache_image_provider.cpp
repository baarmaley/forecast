#include "cache_image_provider.hpp"

#include <file_service/file_service.hpp>

#include <QDebug>

namespace forecast::gui {
CacheImageProvider::CacheImageProvider(FileService& fileService)
    : QQuickImageProvider(QQuickImageProvider::Pixmap), fileService(fileService)
{
}

QPixmap CacheImageProvider::requestPixmap(const QString& id, QSize* size, const QSize& requestedSize)
{
    qDebug() << "load:" << id;

    auto pix = fileService.load(id);

    return pix.scaled(64, 64);
}
} // namespace forecast::gui