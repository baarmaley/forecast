#pragma once

#include <type_url/type_url.hpp>

#include <QString>
#include <QPixmap>

namespace forecast::gui {
class FileService
{
public:
    FileService();

    TypeUrl save(QString dir, std::string bytes);
    
	void remove(QString path);

    QPixmap load(QString path);

private:
    QString generateFileName();

    std::uint64_t countSaveFile{0};
	QString forbiddenErrorResourcePath;
	QPixmap forbiddenError;
    QPixmap notFoundError;
};
} // namespace forecast::gui