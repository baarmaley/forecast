#include "file_service.hpp"

#include <QDebug>
#include <QDir>
#include <QFile>

namespace forecast::gui {
namespace {
constexpr char forbidden_error[] = "forbidden_error.png";
constexpr char not_found_error[] = "404_error.png";
constexpr char path_pattern[]    = "%1/cache/%2";
constexpr char file_pattern[]    = "%1/%2";
constexpr char url_pattern[]     = "%1/%2";
} // namespace

FileService::FileService()
    : forbiddenErrorResourcePath(QString("qrc:/%1").arg(forbidden_error)),
      forbiddenError(QString(":/%1").arg(forbidden_error)),
      notFoundError(QString(":/%1").arg(not_found_error))
{
}

TypeUrl FileService::save(QString dir, std::string bytes)
{
    QString pathCache = QString(path_pattern).arg(QDir::currentPath()).arg(dir);
    QDir cacheDir(pathCache);

    if (!cacheDir.exists() && !cacheDir.mkpath(pathCache)) {
        return ResourceUrl{forbiddenErrorResourcePath};
    }

    auto fileName = generateFileName();

    QFile cacheFile(QString(file_pattern).arg(pathCache).arg(fileName));
    if (!cacheFile.open(QIODevice::WriteOnly)) {
        return ResourceUrl{forbiddenErrorResourcePath};
    }

    cacheFile.write(QByteArray::fromBase64(QByteArray(bytes.c_str(), bytes.size())));
    cacheFile.flush();
    cacheFile.close();

    return ProviderUrl{QString(url_pattern).arg(dir).arg(fileName)};
}

void FileService::remove(QString path)
{
    qDebug() << "remove:" << path;
    auto filePath = QString(path_pattern).arg(QDir::currentPath()).arg(path);
    QFile cacheFile(filePath);
    cacheFile.remove();
}

QPixmap FileService::load(QString path)
{
    auto filePath = QString(path_pattern).arg(QDir::currentPath()).arg(path);
    QFile cacheFile(filePath);

    qDebug() << filePath;

    if (!cacheFile.exists()) {
        return notFoundError;
    }

    if (cacheFile.open(QIODevice::ReadOnly)) {
        QPixmap result;

        if (!result.loadFromData(cacheFile.readAll(), "PNG")) {
            return forbiddenError;
        }

        return result;
    }

    return forbiddenError;
}

QString FileService::generateFileName()
{
    ++countSaveFile;
    return QString("cache_file_%1").arg(countSaveFile);
}
} // namespace forecast::gui