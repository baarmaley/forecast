#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>

#include <model/forecast_model.hpp>

#include <image_cache/flag_cache.hpp>
#include <image_cache/icon_cache.hpp>

#include <cache_provider/cache_image_provider.hpp>

#include <QDebug>
#include <QDir>

#include <QSslSocket>
#include <QWebSocket>

using namespace forecast;

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);
    Q_INIT_RESOURCE(resource);
    QApplication::setApplicationName("Forecast");

    QQmlApplicationEngine engine;

    QQuickStyle::setStyle("Material");

    qDebug() << QDir::currentPath();

    forecast::gui::ServerService serverService;
    serverService.connectToServer();

    forecast::gui::FileService fileService;

    forecast::gui::FlagCache flagCache(serverService, fileService);
    forecast::gui::IconCache iconCache(serverService, fileService);

    forecast::gui::ForecastModel forecastModel(flagCache, iconCache);

    common::SubscribersContainer subscribers;
    subscribers += serverService.helloResponse().subscribe(
        [&](std::vector<core::api::ForecastData> items) { forecastModel.forecastList()->setItems(std::move(items)); });

	subscribers += serverService.rxStatistics().subscribe(
        [&](gui::CompressStatistics s) { forecastModel.setRxStatistics(std::move(s)); });
	subscribers += serverService.txStatistics().subscribe(
		[&](gui::CompressStatistics s) { forecastModel.setTxStatistics(std::move(s)); });


	engine.addImageProvider(forecast::gui::name_cache_image_provider,
                            new forecast::gui::CacheImageProvider(fileService));

    auto ctxt = engine.rootContext();
    ctxt->setContextProperty("forecastModel", &forecastModel);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    // auto rootObjects = engine.rootObjects();
    // if (!rootObjects.isEmpty()) {
    //    auto stackView = rootObjects[0]->findChild<QObject*>("stackView");
    //    if (stackView) {
    //        QObject::connect(stackView, SIGNAL(clickedRelay(int, int)), &commandHelper, SLOT(singleCommand(int,
    //        int)));
    //    } else {
    //        qWarning() << "StackView does not found.";
    //    }
    //}
    // auto stackView = rootObject->findChild<QObject*>("mainStack");
    // qDebug() << rootObject->children();
    // for (const auto& item : rootObjects) {
    //    qDebug() << item << item->objectName();
    //}

    return app.exec();
}
