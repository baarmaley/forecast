#pragma once

#include <api/flag_id.hpp>
#include <file_service/file_service_fwd.hpp>
#include <image_cache/image_cache_fwd.hpp>
#include <server_service/server_service_fwd.hpp>

namespace forecast::gui {
using FlagCache = ImageCache<ServerService, FileService, core::api::FlagId>;
}
