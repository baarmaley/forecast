#pragma once

#include <api/icon_id.hpp>
#include <file_service/file_service_fwd.hpp>
#include <image_cache/image_cache_fwd.hpp>
#include <server_service/server_service_fwd.hpp>

namespace forecast::gui {
using IconCache = ImageCache<ServerService, FileService, core::api::IconId>;
}
