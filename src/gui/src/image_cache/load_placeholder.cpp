#include "load_placeholder.hpp"

namespace forecast::gui::cache {
QString loadPlaceholder(forecast::core::api::FlagId)
{
    return QStringLiteral("qrc:/flag_load_placeholder.png");
}
QString loadPlaceholder(forecast::core::api::IconId)
{
    return QStringLiteral("qrc:/icon_load_placeholder.png");
}
} // namespace forecast::gui::cache