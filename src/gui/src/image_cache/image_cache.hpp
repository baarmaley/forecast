#pragma once

#include <common/context.hpp>
#include <common/event.hpp>
#include <common/overloaded.hpp>

#include <api/request_id.hpp>

#include <cache_provider/name_cache_image_provider.hpp>
#include <type_url/type_url.hpp>

#include <image_cache/error_placeholder.hpp>
#include <image_cache/load_placeholder.hpp>

#include <QDebug>
#include <QTimer>
#include <QUuid>

#include <chrono>
#include <optional>
#include <unordered_map>
#include <variant>

namespace forecast::gui {
template<typename ServerServiceType, typename FileServiceType, typename Key>
class ImageCache
{
public:
    struct EntryCache
    {
        EntryCache(TypeUrl url, std::chrono::seconds expire)
            : url(std::move(url)), registration(std::chrono::steady_clock::now()), expire(std::move(expire))
        {
        }

        TypeUrl url;
        std::chrono::steady_clock::time_point registration;
        std::chrono::seconds expire;
    };
    struct StateCache
    {
        StateCache(core::api::RequestId requestId) : state(std::move(requestId)) {}
        StateCache(EntryCache entryCache) : state(std::move(entryCache)) {}
        std::variant<core::api::RequestId, EntryCache> state;
    };

    ImageCache(ServerServiceType& serverService, FileServiceType& fileService)
        : serverService(serverService),
          fileService(fileService),
          currentSessionDir(QUuid::createUuid().toString(QUuid::WithoutBraces))
    {
        run_timer();
    }

    ImageCache(const ImageCache&) = delete;
    ImageCache(ImageCache&&)      = delete;

    ImageCache& operator=(const ImageCache&) = delete;
    ImageCache& operator=(ImageCache&&) = delete;

    QString getUrl(Key key)
    {
        auto entryCache = getEntryCache(key);

        if (!entryCache) {
            core::api::RequestId requestId = serverService.request(
                key,
                [key, ctx_ptr = ctx.ptr()](std::string bytes, std::chrono::seconds expired) {
                    auto self = ctx_ptr.lock();
                    if (!self) {
                        return;
                    }
                    auto entryCache = (*self)->getEntryCache(key);
                    if (!entryCache) {
                        qWarning() << "Key not found in states.";
                        return;
                    }

                    auto url = (*self)->fileService.save((*self)->currentSessionDir, std::move(bytes));

                    (*entryCache).get() = EntryCache(std::move(url), std::move(expired));
                    (*self)->updateEvent(key);
                },
                [key, ctx_ptr = ctx.ptr()](std::string what) {
                    auto self = ctx_ptr.lock();
                    if (!self) {
                        return;
                    }
                    auto entryCache = (*self)->getEntryCache(key);
                    qWarning() << "on_error:" << what.c_str();
                    if (!entryCache) {
                        qWarning() << "Key not found in states.";
                        return;
                    }
                    (*entryCache).get() =
                        EntryCache(ResourceUrl{forecast::gui::cache::errorPlaceholder(key)}, std::chrono::seconds{5});
                    (*self)->updateEvent(key);
                });

            states.emplace(std::piecewise_construct, std::forward_as_tuple(key), std::forward_as_tuple(requestId));
            return forecast::gui::cache::loadPlaceholder(key);
        }

        auto result = std::visit(
            common::overloaded{[&](const core::api::RequestId&) { return forecast::gui::cache::loadPlaceholder(key); },
                               [&](const EntryCache& entryCache) { return getUrl(entryCache.url); }},
            (*entryCache).get().state);

        return result;
    }

    inline const common::Event<Key>& update() const
    {
        return updateEvent;
    }

private:
    void run_timer()
    {
        QTimer::singleShot(
            std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::seconds{2}).count(),
            [ctx_ptr = ctx.ptr()] {
                auto self = ctx_ptr.lock();
                if (!self) {
                    return;
                }
                auto now = std::chrono::steady_clock::now();
                std::vector<Key> keys_expired;
                for (const auto& [key, value] : (*self)->states) {
                    std::visit(common::overloaded{[key = key, &now, &keys_expired](const EntryCache& entryCache) {
                                                      if (now > (entryCache.registration + entryCache.expire)) {
                                                          keys_expired.push_back(key);
                                                      }
                                                  },
                                                  [](auto) {}},
                               value.state);
                }
                for (const auto& key : keys_expired) {
                    auto it = (*self)->states.find(key);
                    std::visit(common::overloaded{[&self](const EntryCache& entryCache) {
                                                      (*self)->removeFile(entryCache.url);
                                                  },
                                                  [](auto) {}},
                               it->second.state);
                    (*self)->states.erase(it);
                    (*self)->updateEvent(key);
                }
                (*self)->run_timer();
            });
    }

    QString getUrl(const TypeUrl& url) const
    {
        return std::visit(
            common::overloaded{[this](const ProviderUrl& v) {
                                   return QString("image://%1/%2").arg(name_cache_image_provider).arg(v.url);
                               },
                               [](const ResourceUrl& v) { return v.url; }},
            url);
    }

    void removeFile(const TypeUrl& url)
    {
        std::visit(
            common::overloaded{[this](const ProviderUrl& v) { fileService.remove(v.url); }, [](const ResourceUrl&) {}},
            url);
    }

    std::optional<std::reference_wrapper<StateCache>> getEntryCache(Key key)
    {
        auto it = states.find(key);
        if (it == states.end()) {
            return std::nullopt;
        }
        return std::ref(it->second);
    }

    ServerServiceType& serverService;
    FileServiceType& fileService;
    QString currentSessionDir;
    std::unordered_map<Key, StateCache> states;
    common::Event<Key> updateEvent;
    common::Context<ImageCache> ctx{this};
};
} // namespace forecast::gui