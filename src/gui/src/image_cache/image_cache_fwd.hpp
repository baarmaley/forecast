#pragma once

namespace forecast::gui {
template<typename ServerService, typename FileService, typename Key>
class ImageCache;
} // namespace forecast::gui