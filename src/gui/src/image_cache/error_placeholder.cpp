#include "load_placeholder.hpp"

namespace forecast::gui::cache {
QString errorPlaceholder(forecast::core::api::FlagId)
{
    return QStringLiteral("qrc:/flag_error_placeholder.png");
}
QString errorPlaceholder(forecast::core::api::IconId)
{
    return QStringLiteral("qrc:/icon_error_placeholder.png");
}
} // namespace forecast::gui::cache