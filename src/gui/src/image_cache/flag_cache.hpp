#pragma once

#include <api/flag_id.hpp>
#include <file_service/file_service.hpp>
#include <image_cache/image_cache.hpp>
#include <server_service/server_service.hpp>

namespace forecast::gui {
using FlagCache = ImageCache<ServerService, FileService, core::api::FlagId>;
}
