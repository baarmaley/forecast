#pragma once

#include <api/flag_id.hpp>
#include <api/icon_id.hpp>

#include <QString>

namespace forecast::gui::cache {
QString errorPlaceholder(forecast::core::api::FlagId);
QString errorPlaceholder(forecast::core::api::IconId);
} // namespace forecast::gui::cache