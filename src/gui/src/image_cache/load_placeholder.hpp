#pragma once

#include <api/flag_id.hpp>
#include <api/icon_id.hpp>

#include <QString>

namespace forecast::gui::cache {
QString loadPlaceholder(forecast::core::api::FlagId);
QString loadPlaceholder(forecast::core::api::IconId);
} // namespace forecast::gui::cache