import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12

ApplicationWindow
{
    visible: true
    width: 640
    height: 480
    title: Qt.application.name
    property var kbytes: function(bytes){
        return (bytes > 1024) ? Math.ceil(bytes / 1024) + " кбайт" : bytes + " байт";
    }
    property var compress: function(real, compress){
        if(real === 0){
            return "0%"
        }
        return Math.ceil(compress * 100 / real) + "%"
    }

    Rectangle{
        anchors.fill: parent
        anchors.margins: 5
        ColumnLayout{
            anchors.fill: parent
            RowLayout{
                Label{
                    text: "Размер отправленных данных (без/сжатием): " + forecastModel.tx_real + " байт/" + forecastModel.tx_compress + " sбайт"
                }
                Label{
                    text: "Сжатие: " + compress(forecastModel.tx_real, forecastModel.tx_compress)
                }
            }
            RowLayout{
                Label{
                    text: "Размер полученных данных (без/сжатием): " + kbytes(forecastModel.rx_real) + "/" + kbytes(forecastModel.rx_compress)
                }
                Label{
                    text: "Сжатие: " + compress(forecastModel.rx_real, forecastModel.rx_compress)
                }
            }
            ListView{
                model: forecastModel.forecastList
                clip: true
                Layout.fillWidth: true
                Layout.fillHeight: true
                spacing: 2
                delegate: Rectangle{
                    id: rootDelegate
                    width: parent.width
                    height: 80
                    border{ color: "gray"; width: 1}
                    RowLayout{
                        anchors.fill: parent
                        spacing: 2
                        Image {
                            width: rootDelegate.height
                            height: rootDelegate.height
                            source: model.fm_flag_id
                        }
                        Image {
                            width: rootDelegate.height
                            height: rootDelegate.height
                            source: model.fm_icon_id
                        }
                        Label{
                            text: model.fm_name_country + ": " + fm_temperature
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pointSize: 22
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                        }
                    }
                }

            }
        }
    }
}

