#include <catch2/catch.hpp>

#include <api/icon_id.hpp>
#include <image_cache/image_cache.hpp>
#include <api/request_id.hpp>

#include <QApplication>

using namespace forecast;
using namespace forecast::gui;

constexpr char saveFileName[] = "my_save_image";
constexpr char fullFileName[] = "image://cache_image_provider/my_save_image";

constexpr core::api::IconId zero_id{0};
constexpr core::api::IconId one_id{1};
constexpr core::api::IconId two_id{2};

constexpr std::chrono::seconds one_expire{10};

struct FakeServerService
{
    FakeServerService() : run([]() {}) {}

    template<typename Request>
    core::api::RequestId request(Request request,
                      std::function<void(std::string bytes, std::chrono::seconds expire)> on_success,
                      std::function<void(std::string)> on_error)
    {
        request_id = static_cast<core::api::RequestId>(static_cast<std::uint32_t>(request_id) + 1);
        ++count_request;

        if (request == forecast::core::api::IconId{1}) {
            auto new_run = [run = std::move(run), on_success = std::move(on_success)] {
                on_success("", one_expire);
                run();
            };
            run = std::move(new_run);
        } else if (request == forecast::core::api::IconId{2}) {
            auto new_run = [run = std::move(run), on_error = std::move(on_error)] {
                on_error("test");
                run();
            };

            run = std::move(new_run);
        }

        return request_id;
    }

	core::api::RequestId request_id{0};
    std::uint32_t count_request{0};

    std::function<void()> run;
};

struct FakeFileService
{
    gui::TypeUrl save(QString /*dir*/, std::string /*bytes*/)
    {
        return gui::ProviderUrl{saveFileName};
    }

    void remove(QString /*path*/)
    {
        file_removed = true;
    }

    bool file_removed = false;
};

/*
Сценарий:
1. Для одинаковых id будет только один запрос к серверу
2. Для разных id будет выполнено несколько запросов к серверу
3. Для запросов к серверу, которые закончились успехом или ошибкой, будет вызван update
4. Для успешных запросов, вернется url к файлу
5. Для записей, которых уже есть запись в кэше, вернетс¤ кэш и не будет запроса к серверу
6. Для запросов с ошибкой, вернется url к картинке с ошибкой
*/

TEST_CASE("Business logic", "[business_logic][cache]")
{
    FakeServerService fakeServerService;
    FakeFileService fakeFileService;
    ImageCache<FakeServerService, FakeFileService, forecast::core::api::IconId> imageIcon(fakeServerService,
                                                                                          fakeFileService);

    // 1.
    imageIcon.getUrl(zero_id);
    imageIcon.getUrl(zero_id);

    REQUIRE(fakeServerService.count_request == 1);

    // 2.
    imageIcon.getUrl(one_id);
    imageIcon.getUrl(two_id);

    REQUIRE(fakeServerService.count_request == 3);

    // 3.
    std::uint32_t count_upadate_event{0};

    auto subscribe = imageIcon.update().subscribe(
        [&count_upadate_event](const forecast::core::api::IconId&) { ++count_upadate_event; });

    fakeServerService.run();

    // 4.
    REQUIRE(count_upadate_event == 2);

    auto url_1 = imageIcon.getUrl(one_id);

    REQUIRE(url_1 == fullFileName);

    // 5.
    url_1 = imageIcon.getUrl(one_id);

    REQUIRE(fakeServerService.count_request == 3);
    REQUIRE(url_1 == fullFileName);

    // 6.
    auto url_2 = imageIcon.getUrl(two_id);

    REQUIRE(url_2 == forecast::gui::cache::errorPlaceholder(two_id));
}
/*
Cценарий:
1. Cоздаем два запроса на сервер, при старте очереди, выполн¤ем запросы
2. Ждем update(), когда срок хранения данных в кэше истечет дл¤ двух различных записей
3. Останавливаем очередь
4. Делаем запросы в кэш, и проверяем счетчик запросов
5. Проверяем был удален файл или нет
*/

TEST_CASE("Business logic into queue app", "[business_logic][queue_app][cache]")
{
    auto start_tp = std::chrono::steady_clock::now();

    int argc     = 1;
    char* argv[] = {{"test"}};
    QCoreApplication app(argc, argv);

    FakeServerService fakeServerService;
    FakeFileService fakeFileService;
    ImageCache<FakeServerService, FakeFileService, forecast::core::api::IconId> imageIcon(fakeServerService,
                                                                                          fakeFileService);

    std::uint32_t count_one_update{0};
    std::uint32_t count_two_update{0};

    // 1.
    imageIcon.getUrl(one_id);
    imageIcon.getUrl(two_id);

    auto subscribe = imageIcon.update().subscribe([&](const forecast::core::api::IconId& id) {
        // 2.
        if (id == one_id) {
            ++count_one_update;
        }

        if (id == two_id) {
            ++count_two_update;
        }

        if (count_one_update == 2 && count_two_update == 2) {
            app.exit();
        }
    });

    QTimer::singleShot(0, [&] { fakeServerService.run(); });

    app.exec();

    // auto c = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - start_tp).count();
    // qInfo() << c;

    imageIcon.getUrl(one_id);
    imageIcon.getUrl(two_id);

    REQUIRE(fakeServerService.count_request == 4);

    // 5.
    REQUIRE(fakeFileService.file_removed);
}

TEST_CASE("Lifetime", "[lifetime][cache]")
{
    FakeServerService fakeServerService;
    FakeFileService fakeFileService;
    {
        ImageCache<FakeServerService, FakeFileService, forecast::core::api::IconId> imageIcon(fakeServerService,
                                                                                              fakeFileService);
        imageIcon.getUrl(one_id);
        imageIcon.getUrl(two_id);
    }

    fakeServerService.run();

    REQUIRE(true);
}
