#include <catch2/catch.hpp>

#include <common/event.hpp>

using namespace forecast::common;

TEST_CASE("it should does compile")
{
    Event<std::string> single_event;
    Event<std::string, std::uint32_t, std::uint16_t> multi_event;

    std::uint32_t number_of_single_event{0};
    std::uint32_t number_of_multi_event{0};

	auto do_event = [&] {
		single_event("hello");
		multi_event("hello", std::uint32_t{ 32 }, std::uint16_t{ 16 });
	};

	{
    SubscribersContainer subscribers;

    subscribers += single_event.subscribe([&number_of_single_event](const std::string&) { ++number_of_single_event; });
    subscribers +=
        multi_event.subscribe([&number_of_multi_event](const std::string&, const std::uint32_t&, const std::uint16_t&) {
            ++number_of_multi_event;
        });

	do_event();
	}

	do_event();

	REQUIRE(number_of_single_event == 1);
	REQUIRE(number_of_multi_event == 1);

}