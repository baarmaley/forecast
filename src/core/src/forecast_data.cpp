#include <api/forecast_data.hpp>

namespace forecast::core::api {
void to_json(nlohmann::json& j, const ForecastData& forecastData)
{
    j = nlohmann::json{{"name_country", forecastData.name_country},
                       {"temperature", forecastData.temperature},
                       {"flag_id", forecastData.flag_id},
                       {"icon_id", forecastData.icon_id}};
}
void from_json(const nlohmann::json& j, ForecastData& request)
{
    j.at("name_country").get_to(request.name_country);
    j.at("temperature").get_to(request.temperature);
    j.at("flag_id").get_to(request.flag_id);
    j.at("icon_id").get_to(request.icon_id);
}
} // namespace forecast::core::api