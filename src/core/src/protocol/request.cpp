#include <protocol/request.hpp>

#include <common/overloaded.hpp>

#include <protocol/alias.hpp>

namespace forecast::core::protocol {
Request request(api::RequestId id, Hello)
{
    return Request(id, hello_type, Hello{});
}
Request request(api::RequestId id, api::FlagId flag_id)
{
    return Request(id, flag_type, flag_id);
}
Request request(api::RequestId id, api::IconId icon_id)
{
    return Request(id, icon_type, icon_id);
}
void to_json(nlohmann::json& j, const Request& request)
{
    std::uint32_t data = std::visit(common::overloaded{[](Hello) { return std::uint32_t{0}; },
                                                       [](const auto& id) { return static_cast<std::uint32_t>(id); }},
                                    request.data);
    j = nlohmann::json{{"requestId", request.requestId}, {"type", request.type}, {"data", data}};
}
void from_json(const nlohmann::json& j, Request& request)
{
    j.at("requestId").get_to(request.requestId);

    auto type = j.at("type");

    if (type == hello_type) {
    } else if (type == flag_type) {
        api::FlagId flag_id;
        j.at("data").get_to(flag_id);
        request.data = flag_id;
    } else if (type == icon_type) {
        api::IconId icon_id;
        j.at("data").get_to(icon_id);
        request.data = icon_id;
    }
}
} // namespace forecast::core::protocol