#include <protocol/response.hpp>

#include <common/overloaded.hpp>

#include <protocol/alias.hpp>

namespace forecast::core::protocol {
Response response(api::RequestId id, std::vector<core::api::ForecastData> data)
{
    return Response(id, hello_type, HelloResponse{std::move(data)});
}
Response response(api::RequestId id, api::FlagId flag_id, std::chrono::seconds expire, std::string data)
{
    return Response(id, flag_type, FlagResponse{flag_id, expire, std::move(data)});
}
Response response(api::RequestId id, api::IconId icon_id, std::chrono::seconds expire, std::string data)
{
    return Response(id, icon_type, IconResponse{icon_id, expire, std::move(data)});
}
void to_json(nlohmann::json& j, const FlagResponse& flagResponse)
{
    j = nlohmann::json{
        {"flag_id", flagResponse.flagId}, {"expire", flagResponse.expire.count()}, {"data", flagResponse.data}};
}
void from_json(const nlohmann::json& j, FlagResponse& flagResponse)
{
    j.at("flag_id").get_to(flagResponse.flagId);
    std::uint32_t count{0};
    j.at("expire").get_to(count);
    flagResponse.expire = std::chrono::seconds{count};
    j.at("data").get_to(flagResponse.data);
}
void to_json(nlohmann::json& j, const IconResponse& iconResponse)
{
    j = nlohmann::json{
        {"icon_id", iconResponse.iconId}, {"expire", iconResponse.expire.count()}, {"data", iconResponse.data}};
}
void from_json(const nlohmann::json& j, IconResponse& iconResponse)
{
    j.at("icon_id").get_to(iconResponse.iconId);
    std::uint32_t count{0};
    j.at("expire").get_to(count);
    iconResponse.expire = std::chrono::seconds{count};
    j.at("data").get_to(iconResponse.data);
}
void to_json(nlohmann::json& j, const HelloResponse& helloResponse)
{
    j = helloResponse.data;
}
void from_json(const nlohmann::json& j, HelloResponse& helloResponse)
{
    helloResponse.data = j.get<std::vector<api::ForecastData>>();
}
void to_json(nlohmann::json& j, const Response& response)
{
    auto data = std::visit(common::overloaded{[](const auto& v) { return nlohmann::json{v}; }}, response.data);

    j = nlohmann::json{{"requestId", response.requestId}, {"type", response.type}, {"data", std::move(data)}};
}
void from_json(const nlohmann::json& j, Response& response)
{
    j.at("requestId").get_to(response.requestId);
    j.at("type").get_to(response.type);

    auto& data = j.at("data").at(0);
    if (response.type == hello_type) {
        HelloResponse helloResponse;
        data.get_to(helloResponse);
        response.data = std::move(helloResponse);
    } else if (response.type == flag_type) {
        FlagResponse flagResponse;
        data.get_to(flagResponse);
        response.data = std::move(flagResponse);
    } else if (response.type == icon_type) {
        IconResponse iconResponse;
        data.get_to(iconResponse);
        response.data = std::move(iconResponse);
    }
}
} // namespace forecast::core::protocol
