namespace forecast::core::protocol {
constexpr char hello_type[] = "hello";
constexpr char flag_type[]  = "flag";
constexpr char icon_type[]  = "type";
} // namespace forecast::core::api