#pragma once

#include <api/flag_id.hpp>
#include <api/forecast_data.hpp>
#include <api/icon_id.hpp>
#include <api/request_id.hpp>

#include <nlohmann/json.hpp>

#include <chrono>
#include <variant>

namespace forecast::core::protocol {
struct FlagResponse
{
    api::FlagId flagId;
    std::chrono::seconds expire;
    std::string data;
};
struct IconResponse
{
    api::IconId iconId;
    std::chrono::seconds expire;
    std::string data;
};
struct HelloResponse
{
    std::vector<api::ForecastData> data;
};

using ResponseDataType = std::variant<FlagResponse, IconResponse, HelloResponse>;

struct Response
{
    Response() = default;
    Response(api::RequestId requestId, std::string type, ResponseDataType data)
        : requestId(requestId), type(std::move(type)), data(std::move(data))
    {
    }
    api::RequestId requestId{0};
    std::string type;
    ResponseDataType data;
};

Response response(api::RequestId id, std::vector<core::api::ForecastData> data);
Response response(api::RequestId id, api::FlagId flag_id, std::chrono::seconds expire, std::string data);
Response response(api::RequestId id, api::IconId icon_id, std::chrono::seconds expire, std::string data);

void to_json(nlohmann::json& j, const FlagResponse& flagResponse);
void from_json(const nlohmann::json& j, FlagResponse& flagResponse);

void to_json(nlohmann::json& j, const IconResponse& iconResponse);
void from_json(const nlohmann::json& j, IconResponse& iconResponse);

void to_json(nlohmann::json& j, const HelloResponse& helloResponse);
void from_json(const nlohmann::json& j, HelloResponse& helloResponse);

void to_json(nlohmann::json& j, const Response& response);
void from_json(const nlohmann::json& j, Response& response);
} // namespace forecast::core::protocol