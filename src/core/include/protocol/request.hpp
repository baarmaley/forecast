#pragma once

#include <api/flag_id.hpp>
#include <api/icon_id.hpp>
#include <api/request_id.hpp>

#include <protocol/hello.hpp>

#include <nlohmann/json.hpp>

#include <variant>

namespace forecast::core::protocol {
using RequestDataType = std::variant<Hello, api::FlagId, api::IconId>;
struct Request
{
    Request() = default;

    Request(api::RequestId requestId, std::string type, RequestDataType data)
        : requestId(requestId), type(std::move(type)), data(std::move(data))
    {
    }
	api::RequestId requestId{0};
    std::string type;
    RequestDataType data{Hello{}};
};

Request request(api::RequestId id, Hello);
Request request(api::RequestId id, api::FlagId flag_id);
Request request(api::RequestId id, api::IconId icon_id);

void to_json(nlohmann::json& j, const Request& request);
void from_json(const nlohmann::json& j, Request& request);
} // namespace forecast::core::api