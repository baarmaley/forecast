#pragma once

#include <cstdint>

namespace forecast::core::api {
enum class RequestId : std::uint32_t
{
};
} // namespace forecast::gui