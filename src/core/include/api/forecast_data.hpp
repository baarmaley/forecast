#pragma once

#include <api/flag_id.hpp>
#include <api/icon_id.hpp>

#include <nlohmann/json.hpp>

#include <string>

namespace forecast::core::api {
struct ForecastData
{
    std::string name_country;
	float temperature{0};
	FlagId flag_id{0};
	IconId icon_id{0};
};

void to_json(nlohmann::json& j, const ForecastData& request);
void from_json(const nlohmann::json& j, ForecastData& request);
} // namespace forecast::core::api