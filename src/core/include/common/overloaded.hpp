#pragma once

namespace forecast::common {
// clang-format off
template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;
// clang-format on
} // namespace forecast::common