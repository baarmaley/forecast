#pragma once

#include <boost/signals2.hpp>

namespace forecast::common {
namespace s2 = boost::signals2;
/*
Event - ������ ����� ���������� (�� ����� ������) ��������� ��� signals2,
������� �� �������� ������������ �������, ������� ��� (Event) ����������.

������� � ������ connect(), ������� �� const, ������� ��� �������� ��� ������ ����� ���������� mut ref,
��� ��������� ������������ ������� ��� ������.

const_cast<> ���������, �� ��� ��� ���� ������ �� ����� �������� ��� const
��������:
    const Event e;
    e.subscribe() -> UB
*/
template<typename... Ts>
class Event
{
public:
    using SignalType = s2::signal<void(const Ts&...)>;

    Event()  = default;
    ~Event() = default;

    Event(const Event&) = delete;
    Event(Event&&)      = delete;

    Event& operator=(const Event&) = delete;
    Event& operator=(Event&&) = delete;

    void operator()(const Ts&... args) const
    {
        innerSignal(args...);
    }

    s2::connection subscribe(typename SignalType::slot_type slot) const
    {
        return const_cast<SignalType*>(&innerSignal)->connect(std::move(slot));
    }

protected:
    SignalType innerSignal;
};

class SubscribersContainer
{
public:
    SubscribersContainer() = default;

    SubscribersContainer(const SubscribersContainer&) = delete;
    SubscribersContainer(SubscribersContainer&&)      = default;
    SubscribersContainer& operator=(const SubscribersContainer&) = delete;
    SubscribersContainer& operator=(SubscribersContainer&&) = default;

    ~SubscribersContainer() = default;

    void append(s2::scoped_connection&& scoped_connection)
    {
        connection.push_back(std::move(scoped_connection));
    }

    SubscribersContainer& operator+=(s2::scoped_connection&& scoped_connection)
    {
        append(std::move(scoped_connection));
        return *this;
    }

private:
    std::vector<s2::scoped_connection> connection;
};
} // namespace forecast::common